﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;
using System.Drawing;
using System.IO;
using System.Windows.Controls.Primitives;

namespace MediaPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int []trackNumber;
        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;
        private bool userIsDraggingVolumeSlider = false;
        private bool isOpenedTrack = false;
        private bool isTracklistAgain = false;
        private bool isTrackAgain = false;
        private bool randomed = false;
        object []obj;
        object []obj2;
        ToolTip tt1 = new ToolTip();

        public MainWindow()
        {
            InitializeComponent();
            namelist.Items.Clear();
            buttonPause.Visibility = Visibility.Hidden;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((mediaElement.Source != null) && (mediaElement.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
                sliProgress.Value = mediaElement.Position.TotalSeconds;
            }
            if (sliProgress.Value == sliProgress.Maximum)
            {
                if (namelist.SelectedIndex+1 != namelist.Items.Count)
                {
                    if (isTrackAgain == true)
                    {
                        sliProgress.Value = sliProgress.Minimum;
                        mediaElement.Stop();
                        mediaElement.Play();
                        mediaPlayerIsPlaying = true;
                    }
                    else
                    {
                        namelist.SelectedIndex = namelist.SelectedIndex + 1;
                        tracklist.SelectedIndex = namelist.SelectedIndex + 1;
                    }
                }
                else
                {
                    if (isTrackAgain == true)
                    {
                        sliProgress.Value = sliProgress.Minimum;
                        mediaElement.Stop();
                        mediaElement.Play();
                        mediaPlayerIsPlaying = true;
                    }
                    else
                    {
                        namelist.SelectedIndex = 0;
                        tracklist.SelectedIndex = 0;
                        if (isTracklistAgain == false)
                        {
                            mediaElement.Stop();
                            mediaPlayerIsPlaying = false;
                            buttonPause.Visibility = Visibility.Hidden;
                            buttonPlay.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            mediaElement.Stop();
                            mediaElement.Play();
                        }
                    }
                }
            }
        }

        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Media files (*.mp3;*.mpg;*.mpeg)|*.mp3;*.mpg;*.mpeg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                tracklist.Items.Add(new Uri(openFileDialog.FileName));
                namelist.Items.Add(openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf("\\") + 1));
                isOpenedTrack = true;
            }
        }

        private void Play_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (mediaElement != null) && (mediaElement.Source != null);
        }

        private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mediaElement.Play();
            mediaPlayerIsPlaying = true;
        }

        private void Pause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mediaElement.Pause();
        }

        private void Stop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Stop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mediaElement.Stop();
            mediaPlayerIsPlaying = false;
        }

        private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            mediaElement.Position = TimeSpan.FromSeconds(sliProgress.Value);
        }

        private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            if (Convert.ToInt16(TimeSpan.FromSeconds(sliProgress.Value).ToString(@"mm")) > 9)
            {
                labelUpTime.Content = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"mm\:ss");
                labelDownTime.Content = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"mm\:ss") + " / " +
                    mediaElement.NaturalDuration.TimeSpan.Minutes.ToString();
            }
            else
            {
                labelUpTime.Content = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"m\:ss");
                labelDownTime.Content = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"m\:ss") + " / " +
                    mediaElement.NaturalDuration.TimeSpan.Minutes.ToString();
            }
            if (mediaElement.NaturalDuration.TimeSpan.Seconds < 10)
            {
                labelDownTime.Content += ":" + "0" +
                    mediaElement.NaturalDuration.TimeSpan.Seconds.ToString();
            }
            else
            {
                labelDownTime.Content += ":" +
                    mediaElement.NaturalDuration.TimeSpan.Seconds.ToString();
            }
        }

        private void ButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            buttonPause.Visibility = Visibility.Visible;
            buttonPlay.Visibility = Visibility.Hidden;
        }

        private void buttonPause_Click(object sender, RoutedEventArgs e)
        {
            buttonPause.Visibility = Visibility.Hidden;
            buttonPlay.Visibility = Visibility.Visible;
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            buttonPause.Visibility = Visibility.Hidden;
            buttonPlay.Visibility = Visibility.Visible;
        }

        private void sliVolume_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingVolumeSlider = true;
        }

        private void sliVolume_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingVolumeSlider = false;
            mediaElement.Volume = (double)sliVolume.Value;
        }

        private void sliVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (isOpenedTrack)
                mediaElement.Volume = (double)sliVolume.Value;
        }

        private void tracklist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (randomed == true)
            {
                randomed = false;
            }
            else if (namelist.SelectedIndex != -1)
            {
                mediaElement.Source = (Uri)tracklist.Items[namelist.SelectedIndex];
                labelMain.Content = namelist.Items[namelist.SelectedIndex];
            }
        }

        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (namelist.SelectedIndex != -1)
            {
                tracklist.Items.RemoveAt(namelist.SelectedIndex);
                namelist.Items.RemoveAt(namelist.SelectedIndex);
            }
        }

        private void buttonTrackAgain_Click(object sender, RoutedEventArgs e)
        {
            if (isTrackAgain == true)
            {
                isTrackAgain = false;
                buttonTrackAgain.Content = "🔁";
            }
            else
            {
                isTrackAgain = true;
                buttonTrackAgain.Content = "🔂";
            }
        }

        private void buttonRandom_Click(object sender, RoutedEventArgs e)
        {
            trackNumber = new int[namelist.Items.Count];
            for (int i = 0; i < trackNumber.Length; i++)
            {
                trackNumber[i] = 0;
            }
            Random r = new Random();
            int k = 0, selected = namelist.SelectedIndex;
            while (k < trackNumber.Length)
            {
                int temp = r.Next(trackNumber.Length);
                if (trackNumber[temp] == 0)
                {
                    trackNumber[temp] = k;
                    ++k;
                }
            }
            if (namelist.SelectedIndex != -1)
                randomed = true;
            obj = new object[namelist.Items.Count];
            obj2 = new object[namelist.Items.Count];
            int l = 0;
            foreach(object c in tracklist.Items)
            {
                obj[l++] = c;
            }
            l = 0;
            foreach (object c in namelist.Items)
            {
                obj2[l++] = c;
            }

            for (int i = 0; i< trackNumber.Length; i++)
            {
                namelist.Items[i] = obj2[trackNumber[i]];
                tracklist.Items[i] = obj[trackNumber[i]];
            }
            for (int i = 0; i < trackNumber.Length; i++)
            {
                if (trackNumber[i] == selected)
                {
                    namelist.SelectedIndex = i;
                    tracklist.SelectedIndex = i;
                }
            }
        }

        private void buttonListAgain_Click(object sender, RoutedEventArgs e)
        {
            if (isTracklistAgain == true)
            {
                isTracklistAgain = false;
                buttonListAgain.Content = "";
            }
            else
            {
                isTracklistAgain = true;
                buttonListAgain.Content = "";
            }
        }

        private void buttonPrev_Click(object sender, RoutedEventArgs e)
        {
            if (namelist.SelectedIndex > 0)
            {
                namelist.SelectedIndex = namelist.SelectedIndex - 1;
                tracklist.SelectedIndex = namelist.SelectedIndex - 1;
            }
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            if (namelist.SelectedIndex < namelist.Items.Count - 1 && namelist.SelectedIndex > -1)
            {
                namelist.SelectedIndex = namelist.SelectedIndex +1;
                tracklist.SelectedIndex = namelist.SelectedIndex +1;
            }
            else if (namelist.SelectedIndex == namelist.Items.Count - 1)
            {
                namelist.SelectedIndex = 0;
                tracklist.SelectedIndex = 0;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            namelist.Items.Clear();
            tracklist.Items.Clear();
            mediaElement.Stop();
            mediaPlayerIsPlaying = false;
            labelMain.Content = "EMPTY";
            labelUpTime.Content = "0:00";
            labelDownTime.Content = "0:00 / 0:00";
        }

        private void namelist_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (namelist.SelectedIndex != -1)
            {
                mediaElement.Source = (Uri)tracklist.Items[namelist.SelectedIndex];
                mediaElement.Play();
                mediaPlayerIsPlaying = true;
                buttonPause.Visibility = Visibility.Visible;
                buttonPlay.Visibility = Visibility.Hidden;
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Кажиев Серикжан\nВнештатный сотрудник\nКомпьютерной академии \"ШАГ\"\nГруппы СВП-141\nПочта: programmer@gmail.com\nSTEP Inc. 2015", "Об авторе", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void qwe(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите выйти?", "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {

            }
            else e.Cancel = true;
        }

        private void namelist_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ExecutedRoutedEventArgs m = null;
            Open_Executed(sender, m);
        }

        private void Tracklist_Save(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();

            if (save.ShowDialog() == true)
            {
                StreamWriter writer = new StreamWriter(save.FileName);
                foreach(Uri g in tracklist.Items)
                {
                    writer.WriteLine(g);
                }
                writer.Close();
            }
        }

        private void Tracklist_Open(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                string str;
                StreamReader reader = File.OpenText(openFileDialog.FileName);
                while ((str = reader.ReadLine()) != null)
                {
                    tracklist.Items.Add(new Uri(str));
                    namelist.Items.Add(str.Substring(str.LastIndexOf("/") + 1));
                }
                reader.Close();
                isOpenedTrack = true;
            }
        }
    }
}
